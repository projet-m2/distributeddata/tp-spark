#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Template for the job scripts of Spark tutorials."""
import os
import sys
import time

# Define parameters
SPARK_HOME = '/home/nathanael/Documents/DD/TP3 - Spark PUBG/spark-2.4.4-bin-hadoop2.7'
PY4J_ZIP = 'py4j-0.10.7-src.zip'
SPARK_MASTER = 'local[*]'  # Or "spark://[MASTER_IP]:[MASTER_PORT]"
os.environ['PYSPARK_PYTHON'] = 'python3.6'
os.environ['PYSPARK_DRIVER_PYTHON'] = 'python3.6'

# Add pyspark to the path and import this module
sys.path.append(SPARK_HOME + '/python')
sys.path.append(SPARK_HOME + '/python/lib/' + PY4J_ZIP)

from pyspark import SparkConf, SparkContext, StorageLevel  # noqa

# Create a Spark configuration object, needed to create a Spark context
# The master used here is a local one, using every available CPUs
spark_conf = SparkConf().setMaster(SPARK_MASTER)
sc = SparkContext(conf=spark_conf)

#Q18
scorePoints = sc.broadcast([1, 42, 100, 9999])
#Q16
accumulator = sc.accumulator(0)

#Q14
def score(assist, damage, kill, position):
    if kill == 0 or assist / kill >= 5:
        accumulator.add(1)
    score = assist * scorePoints.value[1] + damage * scorePoints.value[0] + kill * scorePoints.value[2]
    if position == 1:
        score += scorePoints.value[3]
    return score

# Define here your processes
try:
    # Load the RDD as a text file and persist it (cached into memory or disk)
    readme_rdd = sc.textFile(os.path.join(SPARK_HOME, '../spark_python/echantillon.csv'))
    readme_rdd.persist()

    #Q14
    start = time.time()
    lines = readme_rdd.map(lambda line: line.split(",")) \
    .filter(lambda head: head[10] != "player_kills") \
    .filter(lambda line: line[11] != "") \
    .map(lambda t: (t[11],  score((float) (t[5]), (float) (t[9]), (float) (t[10]), (float) (t[14])))) \
    .reduceByKey(lambda a, b: a + b) \
    .map(lambda t: (t[1], t[0])) \
    .sortByKey(ascending=False) \
    .take(10)

    for line in lines:
        print(line)

    finish = time.time()
    duration = finish - start
    print("MEMORY_ONLY time (seconds) : %s" % duration)

    print("nombre de fois qu'un joueur a 5 fois plus d'assistance que d'élimination %s" % accumulator.value)

    # Unpersist the readme RDD
    readme_rdd.unpersist()

# Except an exception, the only thing that it will do is to throw it again
except Exception as e:
    raise e

# NEVER forget to close your current context
finally:
    sc.stop()
