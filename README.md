# TP SPARK
##### M2 ILA
##### Nathanael Touchard

## Traitements simples
On collecte seulement 10 résultats car la liste est longue.

**Question 3 :**
```
We have 100000 lines
-------------Q3-------------
(62.0, 'gogolnyg')
(42.0, '651651646')
(38.0, 'appar1008611')
(36.0, 'EsNmToging')
(30.0, 'Kinmmpp')
(25.0, 'MoGu1314')
(25.0, 'asdkmiojfdioe')
(25.0, 'motoMepp')
(24.0, 's1000r-race')
(24.0, 'KouBxczG')
```
**Question 4 :**
```-------------Q4-------------
(62.0, 'gogolnyg')
(42.0, '651651646')
(38.0, 'appar1008611')
(36.0, 'EsNmToging')
(30.0, 'Kinmmpp')
(25.0, 'MoGu1314')
(25.0, 'asdkmiojfdioe')
(25.0, 'motoMepp')
(24.0, 's1000r-race')
(24.0, 'KouBxczG')
```

**Question 5 :**
```-------------Q5-------------
 ((nb kill, nb partie), joueur)
((11.0, 5), 'LawngD-a-w-n-g')
((10.0, 3), 'ILovefantasy')
((9.0, 3), 'Saint_Nick')
((9.0, 3), 'Laissum')
((8.0, 4), 'siliymaui125')
((8.0, 3), 'WSND888')
((8.0, 3), 'CrazyRay123456')
((8.0, 3), 'malelgbb')
((8.0, 3), 'maolanhuodi')
((8.0, 3), 'Mr_shanggan')
```

**Question 6 :**
```-------------Q6-------------
(62.0, 'gogolnyg')
(42.0, '651651646')
(38.0, 'appar1008611')
(36.0, 'EsNmToging')
(30.0, 'Kinmmpp')
(25.0, 'MoGu1314')
(25.0, 'asdkmiojfdioe')
(25.0, 'motoMepp')
(24.0, 's1000r-race')
(24.0, 'KouBxczG')
```

**Question 7 :**
```-------------Q7-------------
(5, 'immj')
(5, 'LawngD-a-w-n-g')
(4, 'killamrie')
(4, 'Koreyoshi364')
(4, 'NerdyMoJo')
(4, 'Roobydooble')
(4, 'Dcc-ccD')
(4, 'crazyone8')
(4, 'China_huangyong')
(4, 'GenOrgg')
(4, 'KBSDUI')
(4, 'siliymaui125')
(4, 'ChanronG')
(4, 'Informaldrip')
(4, 'LILI-F')
(4, 'TemcoEwok')
(4, 'PapaNuntis')
(4, 'JustTuatuatua')
(4, 'dman4771')
```

**Question 8 :**
Il y a un joueur particulier qui n'a pas de pseudo.
```.filter(lambda line: line[11] != "")```

**Question 9 :**
On peut voir que certains joueurs ont beaucoup de kills et très peu de parties. Alors que d'autres on plusieurs parties mais très peu de kills. Comme nous n'avons aucune informations si la partie a été gagné par le joueurs on ne peut pas vraiment déduire s'il faut faire beaucoup de kills ou pas. 

## Persistance
**Question 10 :**
```Nombre total de joueurs : 96667                                                 

-------------Q10-------------
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
```

**Question 11 :**
```.persist(storageLevel=StorageLevel.MEMORY_ONLY)```
Le type de persistance **MEMORY_ONLY** est intéressant. Le temps de calcul CPU est faible, les données sont stockées en mémoire. Il n'utilise pas le disque.
```
96667                                                                           
-------------Q11-------------
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
MEMORY_ONLY time (seconds) : 2.221285581588745
```

**Question 12 :**

Sans persistance avec 3 exécution : `for((i=1;i<4;i++)); do python3 spark_python/q12.py & done`

```96667                                                                           
Sans persist nombre de joueurs : 3.21909499168396
96667                                                                           
Sans persist nombre de joueurs : 3.3291432857513428
96667                                                                           
Sans persist nombre de joueurs : 3.5021419525146484
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.8942022323608398
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.806377649307251
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.7813906669616699
```

Avec persistance
```
96667                                                                           
Sans persist nombre de joueurs : 3.2167165279388428
96667                                                                           
Sans persist nombre de joueurs : 3.3490333557128906
96667                                                                           
Sans persist nombre de joueurs : 3.468719244003296
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.5846254825592041
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.7278430461883545
((11.0, 5), 'LawngD-a-w-n-g')
((8.0, 4), 'siliymaui125')
((7.0, 4), 'Dcc-ccD')
((7.0, 4), 'dman4771')
((6.0, 4), 'NerdyMoJo')
((4.0, 4), 'Roobydooble')
((4.0, 4), 'PapaNuntis')
((3.0, 4), 'JustTuatuatua')
((2.0, 5), 'immj')
((2.0, 4), 'China_huangyong')
Sans persist meilleurs joueurs : 0.576033353805542
```
**Question 13 :**
Au moment de l'exécution des meilleurs joueurs, comme on a sauvegarder les données, elles ne sont pas rechargé donc on gagne du temps à cette exécution.

## Score des joueurs & Variables partagées
**Question 14 à 18**
```
(22332.0, 'UN_JS-9527')                                                         
(22281.0, 'gogolnyg')
(21471.0, 'Dovahkhal')
(21370.0, 'FiscalCliffs')
(20864.0, 'yiyi-520')
(18155.0, '651651646')
(17818.0, 'appar1008611')
(17007.0, 'EsNmToging')
(15628.0, 'Kinmmpp')
(15404.0, 'motoMepp')
MEMORY_ONLY time (seconds) : 2.1988532543182373
nombre de fois qu'un joueur a 5 fois plus d'assistance que d'élimination 58153
```